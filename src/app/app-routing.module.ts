import { DynamicFormsComponent } from './dynamic-forms/dynamic-forms.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [{path:'dynamic-forms/:modules', component:DynamicFormsComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
