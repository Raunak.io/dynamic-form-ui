import { ClothBase } from './clothing.base';


export class TextBox extends ClothBase<string> {
  controlType= 'textbox';
}
