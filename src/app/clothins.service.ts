import { TextBox } from './clothing-textbox';
import { Dropdown } from './clothing-dropdown';
import { ClothBase} from './clothing.base';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';

@Injectable()
export class Service {
  model: string;
  cat = 'category';
  subCat = 'sub-category';
  getCategories(modelVal) {
    console.log('modelvalue to getQue', modelVal);
    this.model = modelVal;
    if (this.model === this.cat) {
      const clothings: ClothBase<string>[] = [
        new Dropdown({
          key: 'status',
          label: 'status',
          options: [
            { key: 'Active', value: 'Active' },
            { key: 'In-Active', value: 'In-Active' },
          ],

          order: 3,
        }),

        new TextBox({
          key: 'categoryCode',
          label: 'Category Code (M|K)',
          required: true,
          order: 1,
        }),
        new TextBox({
          key: 'categoryDescription',
          label: 'Category Description (Men|Kids)',
          required: true,
          order: 2,
        }),
      ];
      console.log(clothings, 'clothings');
      return of(clothings.sort((a, b) => a.order - b.order));
    } else if (this.model === this.subCat) {
      const clothings: ClothBase<string>[] = [
        new Dropdown({
          key: 'status',
          label: 'status',
          options: [
            { key: 'Active', value: 'Active' },
            { key: 'In-Active', value: 'In-Active' },
          ],

          order: 3,
        }),

        new TextBox({
          key: 'subcategoryCode',
          label: 'Sub-Category Code (sht|doh) ',

          required: true,
          order: 1,
        }),
        new TextBox({
          key: 'subcategoryDescription',
          label: 'Sub Category Description (t-shrt|pants)',

          required: true,
          order: 2,
        }),
      ];
      console.log(clothings, 'clothings');
      return of(clothings.sort((a, b) => a.order - b.order));
    }
  }
}
