import {  ClothBase } from './clothing.base';
export class Dropdown extends ClothBase<string> {
  controlType = 'dropdown';
}
