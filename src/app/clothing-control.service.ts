import {  ClothBase } from './clothing.base';
import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Injectable()
export class ControlService {
  constructor() {}
  toFormGroup(dataset: ClothBase<string>[]) {
    const group: any = {};
    dataset.forEach((data) => {
      group[data.key] = data.required
        ? new FormControl(data.value || '', Validators.required)
        : new FormControl(data.value || '');
    });
    return new FormGroup(group);
  }
}
