import { ControlService } from './../clothing-control.service';
import { Observable } from 'rxjs';
import { Service} from '../clothins.service';

import { ClothBase } from '../clothing.base';
import { FormGroup } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'dynamic-forms',
  templateUrl: './dynamic-forms.component.html',
  styleUrls: ['./dynamic-forms.component.css'],
})
export class DynamicFormsComponent implements OnInit {
  clothings: ClothBase<string>[] = [];
  clothing:ClothBase<string>;

  form: FormGroup;
  payLoad = '';
  model: string;
  clothings$: Observable<ClothBase<any>[]>;
  catModel = 'category';
  subCatModel = 'sub-category';
  constructor(
    private service:Service,
    private qcs: ControlService,
    private route: ActivatedRoute
  ) {}
  get isValid() {
    // console.log(this.clothings, 'question key');
    // return this.form.controls[this.question.key].valid;
    return this.form.controls[this.clothing.key].valid;
  }

  ngOnInit() {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('modules')) {
        this.model = paramMap.get('modules');
        console.log('model', this.model);
        if (this.model === this.catModel) {
          this.clothings$ = this.service.getCategories(this.catModel);
          this.clothings$.subscribe((result: ClothBase<string>[]) => {
            this.clothings = result;
            console.log(this.clothings, 'this.questions resuli ---->');
            this.clothings.map((el) => {
              // console.log(el, 'each element');
              this.clothing = el;
            });
            // console.log('this.questions', this.questions);
            // console.log(result, 'result coming from back');
          });
        } else if (this.model === this.subCatModel) {
          this.clothings$ = this.service.getCategories(this.subCatModel);
          this.clothings$.subscribe((result: ClothBase<string>[]) => {
            this.clothings = result;

            this.clothings.map((el) => {
              console.log(el, 'each element');
              this.clothing = el;
            });
            console.log('this.questions', this.clothings);
            // console.log(result, 'result coming from back');
          });
        }
      }
    });

    this.form = this.qcs.toFormGroup(this.clothings);
  }

  onSubmit() {
    this.payLoad = JSON.stringify(this.form.getRawValue());
    console.log(this.form.value, 'this.form value');
  }
}
